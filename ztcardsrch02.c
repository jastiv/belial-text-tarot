/*
Belial Text Tarot - A command line based program for assisting tarot readers in shuffling and reading the cards.
    Copyright (C) 2020 Lori Angela Nagel

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
    
    you can email the author at jastiv@yahoo.com
*/

/*
 * Tarot card table-searching functions.
 *   searchCardNo - find matching Tarot card by number.
 *   searchName - find matching Tarot card by name.
 *  printTcard -print the information on a given tarot card
 * shortprint - only print a tarot cards name and number
 *  read_line - read strings without the scanf problem
 *      tbuf  - flushes the input buffer
 *      tbuf2  - flushes the input buffer for two inputs (not currently used)
 */
#include <stdio.h> 
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "ztcards.h"

PtrToTcard srchTcardNo(PtrToConstTcard ptr, int n, long target)
{
  const PtrToConstTcard endptr = ptr + n;

  for (; ptr < endptr; ptr++)
    if (ptr->number == target)
      return (PtrToTcard) ptr;
  return NULL;
}

PtrToTcard searchTName(PtrToConstTcard ptr, int n, char *target)
{ 
  const PtrToConstTcard endptr = ptr + n;

  for (; ptr < endptr; ptr++)
    if (strcmp(ptr->name, target) == 0)
      return (PtrToTcard) ptr;
  return NULL;
}



void printTcard(PtrToTcard ptr)
{
    printf ("\nName:%25s\n" , ptr->name);
    printf ("\nDescription: %25s\n" , ptr->description);
    printf("The card is no %5d out of the %25s.\n",ptr->suitno, ptr->suit);
    printf("The Deck refrence number is %7d out of 77.\n",ptr->number);
  }
  
void shortprint(PtrToTcard ptr)
{
printf ("Number: %5d" , ptr->number);
    printf ("  Name: %25s\n" , ptr->name);
    
  }


void factorial(int nint)
{
int d;
int k;
printf("this is a test %i \n", nint);
d = nint;
k = nint;
}
 
int read_line(char str[], int n)
{
int ch, i = 0;

while(isspace(ch = getchar()))
;
str[i++] = ch;
while ((ch = getchar()) != '\n') {
if (i < n)
str[i++] = ch;
}
str[i] = '\0';
return i;
}

int tbuf(void)
{
  int   ch;
  /*char  buf[BUFSIZ]; */
  
  puts("Flushing input");
  
  while ((ch = getchar()) != '\n' && ch != EOF);
  
  /*printf ("Enter some text: ");
  
  if (fgets(buf, sizeof(buf), stdin))
  {
    printf ("You entered: %s", buf);
  } */
  
  return 0;
}

int tbuf2(void)
{
  int   ch;
  /*char  buf[BUFSIZ]; */
  
  puts("Got the first monster, \npress return to enter the second..");
  
  while ((ch = getchar()) != '\n' && ch != EOF);
  
  /*printf ("Enter some text: ");
  
  if (fgets(buf, sizeof(buf), stdin))
  {
    printf ("You entered: %s", buf);
  } */
  
  return 0;
}

