/*
Belial Text Tarot - A command line based program for assisting tarot readers in shuffling and reading the cards.
    Copyright (C) 2020 Lori Angela Nagel

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
    
    you can email the author at jastiv@yahoo.com
*/

/*
 * The array of tarot cards to search.
 */
#include <string.h>
#include <stdlib.h>
#include "ztcards.h"

Tcard TcardTable[] =
  { {0, "The Fool",         "Lion Frog in Cave", "Major Arcana", 0},
    {1, "Magician",       "Catperson and Cats", "Major Arcana", 1},
    {2, "High Priestess", "Empty Happy Masks", "Major Arcana", 2},
    {3, "Empress",  "Robed Cheshire Cats", "Major Arcana", 3},
    {4, "Emperor",   "Hearts with Faces in a Box", "Major Arcana", 4},
    {5, "Hierophant",     "Inverted star person", "Major Arcana", 5},
    {6, "The Lovers",       "Cats in and out of Crystal Ball", "Major Arcana", 6},
    {7, "Strength",         "Cat Goddess and Death Machine", "Major Arcana", 7},
    {8, "The Chariot", "Bird Death in Skates", "Major Arcana", 8,},
    {9, "Wheel of Fortune",       "Circle Angel", "Major Arcana", 9},
    {10, "Art", "Monsters in Masks", "Major Arcana", 10},
    {11, "Justice",  "Two Headed Sea Monster", "Major Arcana", 11},
    {12, "Hanged Man",   "Demon Saints", "Major Arcana", 12},
    {13, "Death",     "Two Faced Reaper", "Major Arcana", 13},
    {14, "The Devil",       "Demon Faced Four of Disks", "Major Arcana", 14},
    {15, "The Moon",       "Overlapping Symbols", "Major Arcana", 15},
    {16, "The Star",       "Cats Sticking Out Tounges", "Major Arcana", 16},
    {17, "The Hermit",  "Rainbow Heart Chain", "Major Arcana", 17},
    {18, "The Tower",   "Cup with Lightning", "Major Arcana", 18},
    {19, "The Sun",     "Skulls and Star in Sun", "Major Arcana", 19},
    {20, "Aeon",       "Hourglass Heart Faces", "Major Arcana", 20},
    {21, "The World",       "Hollowed out Candle Flame", "Major Arcana", 21},
    {22, "Page of Wands",         "Wolf with Wand", "Wands", 11},
    {23, "Page of Cups",       "Cat with Cup by Sea", "Cups", 11},
    {24, "Page of Swords", "Dragon with Sword on Ground", "Swords",11},
    {25, "Page of Disks",  "Dragon holding Crecent Potion", "Disks", 11},
    {26, "Knight of Wands",   "Totem Pole with Three Demons", "Wands", 12},
    {27, "Knight of Cups",     "Demon on Circle over Water", "Cups", 12},
    {28, "Knight of Swords",       "Demons with Two Swords", "Swords", 12},
    {29, "Knight of Disks",   "Coin on the Ground", "Disks", 12},
    {30, "Queen of Wands",     "Framing Demons in Wand", "Wands", 13},
    {31, "Queen of Cups",       "Woman on Boat with Three Cats", "Cups", 13},
    {32, "Queen of Swords",     "Woman on Throne in Clouds", "Swords", 13},
    {33, "Queen of Disks",       "Woman on Throne in Woods", "Disks", 13},
    {34, "King of Wands",   "Fire King Cats and Serpent", "Wands", 14},
    {35, "King of Cups",     "King with Advisors", "Cups", 14},
    {36, "King of Swords",         "Grounded Man is a Sword", "Swords", 14},
    {37, "King of Disks",       "Plant man on Throne", "Disks", 14},
    {38, "Ace of Swords", "Cutting the Darkness", "Swords", 1},
    {39, "Two of Swords",  "Shield of Swords on Sign", "Swords", 2},
    {40, "Three of Swords",   "Stabbing a Heart in the Clouds", "Swords", 3},
    {41, "Four of Swords",     "Resting Swords", "Swords", 4},
    {42, "Five of Swords",       "Five Swords in Storm Clouds", "Swords", 5},
    {43, "Six of Swords",   "Swords on a Boat", "Swords", 6},
    {44, "Seven of Swords",     "Swords Slicing Things", "Swords", 7},
    {45, "Eight of Swords",       "Swords Near Mud", "Swords", 8},
    {46, "Nine of Swords",     "Swords Slicing Bed", "Swords", 9},
    {47, "Ten of Swords",       "Swords on Jagged Slab", "Swords", 10},
    {48, "Ace of Wands",   "Wand in Flames", "Wands", 1},
    {49, "Two of Wands",     "Wand Framed Portal", "Wands", 2},
    {50, "Three of Wands",         "Wands with Pryamid", "Wands", 3},
    {51, "Four of Wands",       "Castle Wall with Wands", "Wands", 4},
    {52, "Five of Wands", "Wands shooting at eachother", "Wands", 5},
    {53, "Six of Wands",  "Victory Banner", "Wands", 6},
    {54, "Seven of Wands",   "Wands Standing up to Attack", "Wands", 7},
    {55, "Eight of Wands",     "Flying Wands", "Wands", 8},
    {56, "Nine of Wands",       "Wands in a Fort", "Wands", 9},
    {57, "Ten of Wands",   "Wands on the Ground", "Wands", 10},
    {58, "Ace of Cups",     "Flying Cup over Water", "Cups", 1},
    {59, "Two of Cups",       "Two Cups on a Boat", "Cups", 2},
    {60, "Three of Cups",     "Three Cups on a Table", "Cups", 3},
    {61, "Four of Cups",       "Four Cups Dripping Liquid", "Cups", 4},
    {62, "Five of Cups",   "Cups Spilled but some Upright", "Cups", 5},
    {63, "Six of Cups",     "Cups on a Picnic Cloth", "Cups", 6},
    {64, "Seven of Cups",         "Cups with Foul Things in Them", "Cups", 7},
    {65, "Eight of Cups",       "Cups Getting Rained On", "Cups", 8},
    {66, "Nine of Cups", "Different Cups in Grass", "Cups", 9},
    {67, "Ten of Cups",  "Cups on Plate Under Rainbow", "Cups", 10},
    {68, "Ace of Disks",   "Inverted Pentacule between Ground and Sky", "Disks", 1},
    {69, "Two of Disks",     "Two Disks in the Air", "Disks", 2},
    {70, "Three of Disks",       "Three Disks on a Pyramid", "Disks", 3},
    {71, "Four of Disks",   "Four Disks with Clovers in Them", "Disks", 4},
    {72, "Five of Disks",     "Disks on the Other Side of Water", "Disks", 5},
    {73, "Six of Disks",       "Six Disks on a Rainbow Plate", "Disks", 6},
    {74, "Seven of Disks",     "Seven Disks on two Tree Branches", "Disks", 7},
    {75, "Eight of Disks",       "Eight Disks on a Plate", "Disks", 8},
    {76, "Nine of Disks",   "Nine Disks on a Feather", "Disks", 9},
    {77, "Ten of Disks",     "Ten Disks with Stuff on a Cloth", "Disks", 10}};

const int TcardTableCnt = sizeof(TcardTable)/sizeof(TcardTable[0]);
