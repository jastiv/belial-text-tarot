/*
Belial Text Tarot - A command line based program for assisting tarot readers in shuffling and reading the cards.
    Copyright (C) 2020 Lori Angela Nagel

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
    
    you can email the author at jastiv@yahoo.com
*/


/*
 * Definition of a tarot card.  Tarot cards are not monsters
 * or employees, even though they are all similar. 
 */
#include <stdio.h>
#include <stddef.h>

typedef struct
{
  int   number;
  char   *name;
  char   *description;
  char   *suit;
  int suitno;
} Tcard, *PtrToTcard;

typedef const Tcard *PtrToConstTcard;
