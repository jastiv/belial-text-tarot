/*
Belial Text Tarot - A command line based program for assisting tarot readers in shuffling and reading the cards.
    Copyright (C) 2020 Lori Angela Nagel

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
    
    you can email the author at jastiv@yahoo.com
*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "ztcards.h"

int newldeck(int nums1[], int um1, int nums2[], int um2);
int gnpick = 11;

int decksf10 (){
int tablestu [] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77};
int table2 [77];
int table3 [76];
int table4 [75];
int table5 [74];
int table6 [73];
int table7 [72];
int table8 [71];
int table9 [70];
int table10 [69];
int table11 [68];
/*for (int i= 0; i< 5; i++) */
int i = 0;
printf("This card is signifier.");
newldeck( tablestu, 78, table2, 77);
printf("This card crosses the signifier.");
newldeck( table2, 77, table3, 76);
printf("This card is above me.");
newldeck( table3, 76, table4, 75);
printf("This card is below me.");
newldeck( table4, 75, table5, 74);
printf("This card is behind me.");
newldeck( table5, 74, table6, 73);
printf("This card is in front of me.");
newldeck( table6, 73, table7, 72);
printf("This card shows where I am at.");
newldeck( table7, 72, table8, 71);
printf("This shows what other see.");
newldeck( table8, 71, table9, 70);
printf("This card shows my hopes and fears.");
newldeck( table9, 70, table10, 69);
printf("This card shows the final outcome.");
newldeck( table10, 69, table11, 68);
return 0;
}

int decksf3 (){
int tablestu [] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77};
int table2 [77];
int table3 [76];
int table4 [75];
int table5 [74];
int table6 [73];
int table7 [72];
int table8 [71];
int table9 [70];
int table10 [69];
int table11 [68];
/*for (int i= 0; i< 5; i++) */
int i = 0;
printf("This card is my first pick. \n");
newldeck( tablestu, 78, table2, 77);
printf("This card is my second pick. \n");
newldeck( table2, 77, table3, 76);
printf("This card is my third pick.\n");
newldeck( table3, 76, table4, 75);
return 0;
}


int decksf9 (){
int tablestu [] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77};
int table2 [77];
int table3 [76];
int table4 [75];
int table5 [74];
int table6 [73];
int table7 [72];
int table8 [71];
int table9 [70];
int table10 [69];
int table11 [68];
/*for (int i= 0; i< 5; i++) */
int i = 0;
printf("This card shows my first choice in the past. \n");
newldeck( tablestu, 78, table2, 77);
printf("This card shows my first choice now. \n");
newldeck( table2, 77, table3, 76);
printf("This card shows my first choice in the future. \n");
newldeck( table3, 76, table4, 75);
printf("This card shows my second choice the past. \n");
newldeck( table4, 75, table5, 74);
printf("This card shows my second choice now.\n");
newldeck( table5, 74, table6, 73);
printf("This card shows my second choice in the future. \n");
newldeck( table6, 73, table7, 72);
printf("This card shows the over all situation. \n");
newldeck( table7, 72, table8, 71);
printf("This shows the source of conflict. \n");
newldeck( table8, 71, table9, 70);
printf("This card shows the final resolution. \n");
newldeck( table9, 70, table10, 69);
return 0;
}

int decksplife (){
int tablestu [] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77};
int table2 [77];
int table3 [76];
int table4 [75];
int table5 [74];
int table6 [73];
int table7 [72];
int table8 [71];
int table9 [70];
int table10 [69];
int table11 [68];
/*for (int i= 0; i< 5; i++) */
int i = 0;
printf("This who you were in your past life. \n");
newldeck( tablestu, 78, table2, 77);
printf("This is what you did in your past life. \n");
newldeck( table2, 77, table3, 76);
printf("This was your main challenge in your past life. \n");
newldeck( table3, 76, table4, 75);
printf("This is how you died in your past life. \n");
newldeck( table4, 75, table5, 74);
printf("This is what you learned from your past life.\n");
newldeck( table5, 74, table6, 73);
printf("This is how you have been influnced by your past life. \n");
newldeck( table6, 73, table7, 72);
return 0;
}

int decksf11 (){
int tablestu [] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77};
int table2 [77];
int table3 [76];
int table4 [75];
int table5 [74];
int table6 [73];
int table7 [72];
int table8 [71];
int table9 [70];
int table10 [69];
int table11 [68];
int table12 [67];
/*for (int i= 0; i< 5; i++) */
int i = 0;
printf("This is the signifier. \n");
newldeck( tablestu, 78, table2, 77);
printf("This card crosses the signifier. \n");
newldeck( table2, 77, table3, 76);
printf("This card shows a good reason for moving forward. \n");
newldeck( table3, 76, table4, 75);
printf("This card shows a poor reason for moving forward. \n");
newldeck( table4, 75, table5, 74);
printf("This card shows the past.\n");
newldeck( table5, 74, table6, 73);
printf("This card shows a future influence. \n");
newldeck( table6, 73, table7, 72);
printf("This card shows how the quarent sees the situation. \n");
newldeck( table7, 72, table8, 71);
printf("This shows how others see the situation. \n");
newldeck( table8, 71, table9, 70);
printf("This card show what we hope or fear to happen. \n");
newldeck( table9, 70, table10, 69);
printf("This shows the most likely result. \n");
newldeck( table10, 69, table11, 68);
printf("This card explains the likely result. \n");
newldeck( table11, 68, table12, 67);
return 0;
}

int decksf78 (){
int tablestu [] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77};
int table2 [77];
int table3 [76];
int table4 [75];
int table5 [74];
int table6 [73];
int table7 [72];
int table8 [71];
int table9 [70];
int table10 [69];
int table11 [68];
int table12 [67];
int table13 [66];
int table14 [65];
int table15 [64];
int table16 [63];
int table17 [62];
int table18 [61];
int table19 [60];
int table20 [59];
int table21 [58];
int table22 [57];
int table23 [56];
int table24 [55];
int table25 [54];
int table26 [53];
int table27 [52];
int table28 [51];
int table29 [50];
int table30 [49];
int table31 [48];
int table32 [47];
int table33 [46];
int table34 [45];
int table35 [44];
int table36 [43];
int table37 [42];
int table38 [41];
int table39 [40];
int table40 [39];
int table41 [38];
int table42 [37];
int table43 [36];
int table44 [35];
int table45 [34];
int table46 [33];
int table47 [32];
int table48 [31];
int table49 [30];
int table50 [29];
int table51 [28];
int table52 [27];
int table53 [26];
int table54 [25];
int table55 [24];
int table56 [23];
int table57 [22];
int table58 [21];
int table59 [20];
int table60 [19];
int table61 [18];
int table62 [17];
int table63 [16];
int table64 [15];
int table65 [14];
int table66 [13];
int table67 [12];
int table68 [11];
int table69 [10];
int table70 [9];
int table71 [8];
int table72 [7];
int table73 [6];
int table74 [5];
int table75 [4];
int table76 [3];
int table77 [2];
int table78 [1];
/*for (int i= 0; i< 5; i++) */
int i = 0;
printf("1). ");
newldeck( tablestu, 78, table2, 77);
printf("2). ");
newldeck( table2, 77, table3, 76);
printf("3). ");
newldeck( table3, 76, table4, 75);
printf("4). ");
newldeck( table4, 75, table5, 74);
printf("5). ");
newldeck( table5, 74, table6, 73);
printf("6). ");
newldeck( table6, 73, table7, 72);
printf("7). ");
newldeck( table7, 72, table8, 71);
printf("8). ");
newldeck( table8, 71, table9, 70);
printf("9). ");
newldeck( table9, 70, table10, 69);
printf("10).");
newldeck( table10, 69, table11, 68);
printf("11).");
newldeck( table11, 68, table12, 67);
printf("12).");
newldeck( table12, 67, table13, 66);
printf("13).");
newldeck( table13, 66, table14, 65);
printf("14). ");
newldeck( table14, 65, table15, 64);
printf("15). ");
newldeck( table15, 64, table16, 63);
printf("16). ");
newldeck( table16, 63, table17, 62);
printf("17). ");
newldeck( table17, 62, table18, 61);
printf("18). ");
newldeck( table18, 61, table19, 60);
printf("19). ");
newldeck( table19, 60, table20, 59);
printf("20). ");
newldeck( table20, 59, table21, 58);
printf("21). ");
newldeck( table21, 58, table22, 57);
printf("22). ");
newldeck( table22, 57, table23, 56);
printf("23). ");
newldeck( table23, 56, table24, 55);
printf("24). ");
newldeck( table24, 55, table25, 54);
printf("25). ");
newldeck( table25, 54, table26, 53);
printf("26). ");
newldeck( table26, 53, table27, 52);
printf("27). ");
newldeck( table27, 52, table28, 51);
printf("28). ");
newldeck( table28, 51, table29, 50);
printf("29). ");
newldeck( table29, 50, table30, 49);
printf("30). ");
newldeck( table30, 49, table31, 48);
printf("31). ");
newldeck( table31, 48, table32, 47);
printf("32). ");
newldeck( table32, 47, table33, 46);
printf("33). ");
newldeck( table33, 46, table34, 45);
printf("34). ");
newldeck( table34, 45, table35, 44);
printf("35). ");
newldeck( table35, 44, table36, 43);
printf("36). ");
newldeck( table36, 42, table37, 41);
printf("37). ");
newldeck( table37, 41, table38, 40);
printf("38). ");
newldeck( table38, 40, table39, 39);
printf("39). ");
newldeck( table39, 39, table40, 38);
printf("40). ");
newldeck( table40, 38, table41, 37);
printf("41). ");
newldeck( table41, 37, table42, 36);
printf("42). ");
newldeck( table42, 36, table43, 35);
printf("43). ");
newldeck( table43, 35, table44, 34);
printf("44). ");
newldeck( table44, 34, table45, 33);
printf("45). ");
newldeck( table45, 33, table46, 32);
printf("46). ");
newldeck( table46, 32, table47, 31);
printf("47). ");
newldeck( table47, 31, table48, 30);
printf("48). ");
newldeck( table48, 30, table49, 29);
printf("49). ");
newldeck( table49, 29, table50, 28);
printf("50). ");
newldeck( table50, 28, table51, 27);
printf("51). ");
newldeck( table51, 27, table52, 26);
printf("52). ");
newldeck( table52, 26, table53, 25);
printf("53). ");
newldeck( table53, 25, table54, 24);
printf("54). ");
newldeck( table54, 24, table55, 23);
printf("55). ");
newldeck( table55, 23, table56, 22);
printf("56). ");
newldeck( table56, 22, table57, 21);
printf("57). ");
newldeck( table57, 21, table58, 20);
printf("58). ");
newldeck( table58, 20, table59, 19);
printf("59). ");
newldeck( table59, 19, table60, 18);
printf("60). ");
newldeck( table60, 18, table61, 17);
printf("61). ");
newldeck( table61, 17, table62, 16);
printf("62). ");
newldeck( table62, 16, table63, 15);
printf("63). ");
newldeck( table63, 15, table64, 14);
printf("64). ");
newldeck( table64, 14, table65, 13);
printf("65). ");
newldeck( table65, 13, table66, 12);
printf("66). ");
newldeck( table66, 12, table67, 11);
printf("67). ");
newldeck( table67, 11, table68, 10);
printf("68). ");
newldeck( table68, 10, table69, 9);
printf("69). ");
newldeck( table69, 9, table70, 8);
printf("70). ");
newldeck( table70, 8, table71, 7);
printf("71). ");
newldeck( table71, 7, table72, 6);
printf("72). ");
newldeck( table72, 6, table73, 5);
printf("73). ");
newldeck( table73, 5, table74, 4);
printf("74). ");
newldeck( table74, 4, table75, 3);
printf("75). ");
newldeck( table75, 3, table76, 2);
printf("76). ");
newldeck( table76, 2, table77, 1);
printf("77). ");
newldeck( table77, 1, table78, 0);
printf("78). ");
return 0;
}


int newldeck(int nums1[], int um1, int nums2[], int um2){
int i = 0;

/*while (i < um1 ){
printf("Slot no: %d Card # %d \n", i, nums1[i]);
i++;
}
*/
PtrToTcard srchTcardNo(const Tcard tab[], int n, long target);
void shortprint(PtrToTcard ptr);
extern Tcard TcardTable[];
extern const int TcardTableCnt;
  PtrToTcard matchptr;
unsigned int iseed = (unsigned int)time(NULL);
srand(iseed);
 int genme = (rand() % um1 );

/*printf("Int genme %d \n",genme); */

int kate = nums1[genme];

matchptr = srchTcardNo(TcardTable, TcardTableCnt, kate);
  if (matchptr != NULL){
 /*   printf("%d is in record %p\n",kate, matchptr); */
    shortprint(matchptr);
    }
  else
    printf("%d wasn't found\n",kate);
 
/*printf("this is the card %d \n",nums1[genme]); */
int k = 0;
while (k < um2) {
if ( k < genme)
{
nums2[k] = nums1[k];
}
else {
nums2[k] = nums1[k+1];
}
k++;
}

return nums1[genme];
}
