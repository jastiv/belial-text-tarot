/*
Belial Text Tarot - A command line based program for assisting tarot readers in shuffling and reading the cards.
    Copyright (C) 2020 Lori Angela Nagel

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
    
    you can email the author at jastiv@yahoo.com
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "ztcards.h"
#define MENU 60

int menu()
{
int tbuf();
int credith();
int threecard();
void Tcardlist(PtrToConstTcard ptr, int n);
PtrToTcard Tcardnum();
PtrToTcard rndTcard();
PtrToTcard rndTcard2();
int read_line(char str[], int n);
char mychoice[MENU];
char choice;
printf("The Basic Tarot of C\n");
while (choice != '0')
{

printf("Tarot Reader - choose your cards:\n"
                  "1) Introduction\n"
                  "2) Pick Random Tarot Card\n"
                  "3) Three Card Spread\n"
                  "4) Nine card A vs B Spread\n"
                  "5) Classic Celtic Cross Spread\n"
		  "6) Eleven Card Expanded Celtic Cross\n"
		  "7) List of the Tarot Cards\n"
		  "8) Show a tarot card by its number.\n"
		  "9) Credits\n"
                  "a) Pick all 78 cards Randomly\n"
                  "b) Past Life Six Card Spread\n"
		  "0) Quit reading the tarot cards\n");
read_line(mychoice, MENU);
		  choice = mychoice[0];
switch(choice)
{
case '1':
{
printf("Ever have questions about your life that have no logical or obvious answer?.\n");
printf("Haven't we all.  Make quick insights with pick a random card. \n");
printf("The three card spread can be past present future, or body mind spirit.\n");
printf("A vs B helps you make better choices like what job to take.\n");
printf("The Celtic Cross helps when you need an in depth reading or a situation is complex.\n");
printf("If you are still confused, a proffesional reader can bring insight to a situation or...\n");
printf("Help you confirm your intuition if you are still unsure.\n");
printf("Don't forget to share this program with your friends or anyone you know who has an intrest in Tarot.\n");
}
break;

case '2':
{
printf("I will now pick a random tarot card\n");
rndTcard();
}
break;
case '3':
{
printf("Three Card Spread\n");
decksf3 ();

}
break;
case '4':
{
printf("Nine card First choice vs Second choice Spread\n");
decksf9 ();
tbuf();
}
break;
case '5':
{
printf("Celtic Cross Limited 10 card Spread\n");
decksf10 ();
printf("\n");
}
break;
case '6':
{
printf(" Celtic Cross 11 card Spread.\n");
decksf11 ();
}
break;
case '7':
/*{
extern Tcard TcardTable[];
  extern const int TcardTableCnt;
printf("Tarot Card List - names and numbers\n");
Tcardlist(TcardTable, TcardTableCnt);
}*/
break;
case '8':
{

printf("Display a Tarot card by number.\n");
Tcardnum();
}
break;
case '9':
{
printf("These are the creatures...\n");
printf("who have aided me on my quest...\n");
printf("to write this program...\n");
credith();
}
break;
case 'a':
{
printf("Now we will pick all the cards \n One by One\n");
decksf78 ();
}
break;

case 'b':
{
printf("Six Card Past Life Spread\n \n");
decksplife ();
}
break;
case '0':
printf("Time to quit.\n");
break;

case 't':
{
printf("This is the Test Run of Cards.\n");
int poo = 500;
while (poo > 0){
rndTcard2();
printf("Um %d",poo);
sleep(1);
poo--;
}
}
break;
default:
{
printf("This is the default case.\n");
}
break;
}
}
return 0;
}
    
PtrToTcard Tcardnum()
{
PtrToTcard srchTcardNo(const Tcard tab[], int n, long target);
void printTcard(PtrToTcard ptr);
int tbuf();
extern Tcard TcardTable[];
extern const int TcardTableCnt;
  PtrToTcard matchptr;
  PtrToTcard tcard1;
  int tcardnum;
  printf("Enter the reference number of the tarot card that you would like to display\n");
scanf("%d", &tcardnum);
  matchptr = srchTcardNo(TcardTable, TcardTableCnt, tcardnum);
  if (matchptr != NULL){
    printTcard(matchptr);
    tcard1 = matchptr;
    return tcard1;
    }
  else
    printf("%d wasn't found\n",tcardnum);
   tbuf();
    return NULL;
 }
 
 
 PtrToTcard rndTcard()
 {
 PtrToTcard srchTcardNo(const Tcard tab[], int n, long target);
void printTcard(PtrToTcard ptr);
extern Tcard TcardTable[];
extern const int TcardTableCnt;
  PtrToTcard matchptr;
 printf("Looking for a random tarot card...\n");
 unsigned int iseed = (unsigned int)time(NULL);
srand(iseed);
 int genme = (rand() % TcardTableCnt );
  matchptr = srchTcardNo(TcardTable, TcardTableCnt, genme);
  if (matchptr != NULL){
    printTcard(matchptr);
    return matchptr;
    }
  else
    printf("%d wasn't found\n",genme);
    return NULL;
 }

PtrToTcard rndTcard2()
 {
 PtrToTcard srchTcardNo(const Tcard tab[], int n, long target);
void shortprint(PtrToTcard ptr);
extern Tcard TcardTable[];
extern const int TcardTableCnt;
  PtrToTcard matchptr;
 printf("Looking for a random tarot card...\n");
 unsigned int iseed = (unsigned int)time(NULL);
srand(iseed);
 int genme = (rand() % TcardTableCnt );
  matchptr = srchTcardNo(TcardTable, TcardTableCnt, genme);
  if (matchptr != NULL){
 /*   printf("%d is in record %p\n",genme, matchptr); */
    shortprint(matchptr);
    return matchptr;
    }
  else
    printf("%d wasn't found\n",genme);
    return NULL;
 }

 
 int credith()
 {
 printf("Goetic Daemon Buer..\n");
 printf("..for helping me learn to code in C.\n");
 printf("Belial\n");
 printf(".....For helping me improve this program\n");
 printf("And encouraging me to share it\n");
 printf("..with others even though it looks like a noob wrote it.\n");
 printf("\n");
 return 0;
 }
 
 
 
int main()
{
int z;
z = 1;
do
{
menu();
z = 0;
}while (z == 1);
return 0;
}


/*
int read_line(char str[], int n)
{
int ch, i = 0;

while(isspace(ch = getchar()))
;
str[i++] = ch;
while ((ch = getchar()) != '\n') {
if (i < n)
str[i++] = ch;
}
str[i] = '\0';
return i;
}
*/
